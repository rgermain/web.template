#!/bin/bash

cd $(dirname "$0")

getDockerContainerId () {
    # $1 == name
    CONTAINER_ID=docker ps -q -f name=$1
    return $CONTAINER_ID
}

frontend () {
    echo "[FRONTEND]"
    echo "ID = $(getDockerContainerId frontend)"
    git -C ./frontend pull
    docker-compose up -d --build
    echo "FINAL ID = $(getDockerContainerId frontend)"
}

backend () {
    echo "[BACKEND]"
    echo "ID = $(getDockerContainerId backend)"
    git -C ./backend pull
    docker-compose up -d --build
    echo "FINAL ID = $(getDockerContainerId backend)"
}

all () {
    backend
    frontend
}

for arg in $@; do

    if [ $arg == "frontend" ]; then
        frontend
    elif [ $arg == "backend" ]; then
        backend
    elif [[ $arg == "all" ]]; then
        all
    else
        echo "unknow command $arg..."
        echo "exit..."
        exit 0
    fi
done