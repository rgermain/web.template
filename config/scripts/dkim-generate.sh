#!/bin/bash

mkdir -p ./dkim_keys; cd ./dkim_keys

DOMAINS=()

generate_dkim () {
    opendkim-genkey -b 2048 -h rsa-sha256 -r -v --subdomains -s mail -d $1
    sed -i 's/h=rsa-sha256/h=sha256/' mail.txt
    chmod +r mail.private mail.txt
    mv mail.private $1.private
    mv mail.txt $1.txt
    chmod +r mail.private mail.txt
}

check_file_exist () {
    # $1 == domaine name
    if [ -f "$1.private" ]; then
		read -p "File exsist, do you want to replace it?" yn
        if [ $yn == "y" ]; then
      		generate_dkim $1
        else
            echo "continue..."
		fi
    else
      generate_dkim $1
	fi
}


for DOMAIN in $DOMAINS; do
    # generate both domaine name
    check_file_exist $DOMAIN
    check_file_exist "www.$DOMAIN"
done
