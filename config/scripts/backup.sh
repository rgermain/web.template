#!/bin/bash

CONTAINER=$(docker ps -f name=backend -q)

echo "[ BACKUP DATABASE ]"
docker exec $CONTAINER ./manage.py dbbackup --encrypt --clean --compress
echo "[ BACKUP MEDIA ]"
docker exec $CONTAINER ./manage.py mediabackup --encrypt --clean --compress

echo $GREEN"done"$RESET