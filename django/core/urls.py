from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.contrib.staticfiles.urls import static
from django.conf import settings
import os
from core.drf_routers import urls

router = routers.DefaultRouter()
# load the url of all app

for url in urls:
    router.register(*url)


urlpatterns = [
    # api models
    path('', include(router.urls)),

    # account / auth
    path('auth/', include('authentication.urls')),
]


if settings.DEBUG:

    urlpatterns.append(path('admin/', admin.site.urls))
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    if settings.DEBUG_TOOLBAR:
        import debug_toolbar
        urlpatterns.append(
            path('__debug__/', include(debug_toolbar.urls)),
        )

# for produdction
else:

    # add admin django only if sufffix_admin is set and min length 8 char
    SUFFIX_ADMIN = os.environ.get("SUFFIX_ADMIN", None)
    if SUFFIX_ADMIN and len(SUFFIX_ADMIN) >= 8:
        urlpatterns.append(path(f'admin-{SUFFIX_ADMIN}/', admin.site.urls))
