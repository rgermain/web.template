from django.contrib import admin
from django.conf import settings
from utils.function import contenttypes_uuid
from utils.models import Issue, Commit


class AdminBase(admin.ModelAdmin):
    """
        model base for admin, with glocal function
    """
    empty_value_display = "- empty -"
    list_per_page = 20
