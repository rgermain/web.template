from django.db import models
from django.conf import settings
from lib.manager import QuerySetBase


class ModelBaseAbstract(models.Model):
    """
        chernobyl base models
    """

    objects = QuerySetBase.as_manager()

    class Meta:
        abstract = True
        ordering = ['-id']

    def save(self, *args, **kwargs):
        # override the save for check every time the field
        if settings.DEBUG:
            self.full_clean()
        super(ModelBaseAbstract, self).save(*args, **kwargs)
        return self
