from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework.fields import empty
from django.http import QueryDict


class ListSerializer(serializers.ListSerializer):
    def get_value(self, dictionary):
        # need this for nested parser
        if isinstance(dictionary, QueryDict):
            return dictionary.getlist(self.field_name, empty)
        return super().get_value(dictionary)


class ModelSerializerBaseNested(WritableNestedModelSerializer):

    def __init__(self, *args, **kwargs):
        setattr(self.Meta, 'list_serializer_class', ListSerializer)
        super().__init__(*args, **kwargs)
