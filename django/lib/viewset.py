from rest_framework import viewsets
from lib.parser.parser import NestedParser
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
# from drf_nested_multipart_parser import NestedMultipartParser


class ModelViewSetBase(viewsets.ModelViewSet):
    parser_classes = (NestedParser, MultiPartParser, FormParser, JSONParser)

    def paginate_queryset(self, queryset):
        """
            no paginate if no_page in query params
        """
        if 'no_page' in self.request.query_params:
            return None
        return super().paginate_queryset(queryset)
