export default {
  target: 'server',
  ssr: true,

  telemetry: false,

  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'og:image', content: '/icon.png' },
      { name: 'twitter:image', content: '/icon.png' },
      ...require('~/config/head.config.js').default,
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  /* -------------------------------------------------
    render sections
    ------------------------------------------------- */

  render: {
    http2: {
      push: true,
      pushAssets: null,
    },
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      },
    },
  },
  sitemap: {
    hostname: process.env.SITE_URL,
    gzip: true,
    i18n: true,
    Default: {
      changefreq: 'monthly',
      priority: 1,
      lastmod: new Date(),
    },
  },
  robots: [],

  pwa: {
    meta: {
      theme_color: '#1a202c',
      nativeUI: true,
    },
    manifest: {
      theme_color: '#1a202c',
    },
  },

  /* -------------------------------------------------
    config sections
    ------------------------------------------------- */

  modules: [
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/pwa',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'vue-toastification/nuxt',
    'nuxt-i18n',
    '@nuxtjs/svg-sprite',
  ],

  plugins: [
    '~/plugins/axios.js',
    '~/plugins/auth.js',
    '~/plugins/vuePlugins.client.js',
  ],

  env: {
    BACKEND_URL: process.env.BACKEND_URL || 'http://localhost:8000',
    SITE_URL: process.env.SITE_URL || 'http://localhost:3000',
  },

  /* -------------------------------------------------
     sections
    ------------------------------------------------- */

  i18n: require('~/config/i18n.config.js').default,
  auth: require('~/config/auth.config.js').default,
  axios: {
    baseURL: process.env.BACKEND_URL || 'http://localhost:8000',
  },

  /* -------------------------------------------------
  style sections
  ------------------------------------------------- */

  css: ['~/assets/styles/default.scss'],

  toast: {
    cssFile: '~/assets/styles/toast.scss',
  },

  svgSprite: {
    input: '~/assets/svg',
    output: '~/assets/.svg-compile',
  },

  toast: {
    iconPack: 'callback',
    position: 'top-center',
  },

  tailwindcss: {
    exposeConfig: true,
    jit: true,
  },

  // nuxt color mode (dark mode)
  colorMode: {
    classSuffix: '',
  },

  purgeCSS: {
    whitelistPatterns: [
      // vue-toastification purge
      /Vue-Toastification/,
      /(top|bottom)-(left|center|right)/,
    ],
    keyframes: true,
    variables: true,
    fontFace: true,
  },

  /* -------------------------------------------------
    build sections
    ------------------------------------------------- */

  components: true,

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxt/components',
    'nuxt-purgecss',
    '@nuxtjs/color-mode',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/svg-sprite',
    '@nuxtjs/html-validator',
  ],

  build: {
    html: {
      minify: {
        // fix input type attributes
        removeRedundantAttributes: false,
      },
    },
    postcss: {
      plugins: {
        cssnano: {
          preset: [
            'default',
            {
              discardComments: {
                removeAll: true,
              },
            },
          ],
        },
      },
    },
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)|(\.svg$)/,
        })
      }
    },
  },
}
